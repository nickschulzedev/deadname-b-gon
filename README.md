![](imgs/bones048.png) Deadname-B-Gon replaces a given name with a better one on websites you visit. Or, you can replace as many words or phrases as you like with any other words or phrases you choose. It's tested to guarantee it works well with Gmail and more!

This extension does not use any external Javascript code. This extension's source code is licensed under the following Creative Commons license: [Attribution-NonCommercial-ShareAlike 4.0 International (CC BY-NC-SA 4.0)](https://creativecommons.org/licenses/by-nc-sa/4.0/)

Any bugs you can find, or any other suggestions, comments, criticism, or praise, are greatly appreciated! Please email deadname-b-gon-dev-contact@protonmail.com

[Privacy policy.](https://sites.google.com/view/privacypolicy-deadname-b-gon)

Logo image credit: [Skull and Crossbones Large Pink](https://vector.me/browse/264614/skull_and_crossbones_large_pink) from [Vector.me](https://vector.me/) (by Lil_Mermaid_Girl).

Download Links:
* [Google Chrome](https://chrome.google.com/webstore/detail/deadname-b-gon/bpglollfkjfopjpnhffddkpcemoicela)
* [Microsoft Edge](https://microsoftedge.microsoft.com/addons/detail/pepdepilieehjgphbfmhailmemlpfmgi)
* [Firefox](https://addons.mozilla.org/en-US/firefox/addon/deadname-b-gon/)